/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ultimatetcc;

/**
 *
 * @author Jean Cheiran jfpcheiran@gmail.com
 */
public class Configuracoes {
    
    private static String nomeCoordenadorTCC = "Jean Felipe Patikowski Cheiran";
    private static String generoCoordenadorTCC = "o"; //usar o ou a
    private static String nomeCoordenadorES = "João Pablo Silva da Silva";
    private static String generoCoordenadorES = "o"; //usar o ou a
    private static String nomeCoordenadorCC = "Amanda Meincke Melo";
    private static String generoCoordenadorCC = "a"; //usar o ou a

    /**
     * @return the nomeCoordenadorTCC
     */
    public static String getNomeCoordenadorTCC() {
        return nomeCoordenadorTCC;
    }

    /**
     * @param aNomeCoordenadorTCC the nomeCoordenadorTCC to set
     */
    public static void setNomeCoordenadorTCC(String aNomeCoordenadorTCC) {
        nomeCoordenadorTCC = aNomeCoordenadorTCC;
    }

    /**
     * @return the generoCoordenadorTCC
     */
    public static String getGeneroCoordenadorTCC() {
        return generoCoordenadorTCC;
    }

    /**
     * @param genero the generoCoordenadorTCC to set
     */
    public static void setGeneroCoordenadorTCC(String genero) {
        if(genero.equals("a") || genero.equals("coordenadora")){
            generoCoordenadorTCC = "a";
        }else{
            generoCoordenadorTCC = "o";
        }
    }

    /**
     * @return the nomeCoordenadorES
     */
    public static String getNomeCoordenadorES() {
        return nomeCoordenadorES;
    }

    /**
     * @param aNomeCoordenadorES the nomeCoordenadorES to set
     */
    public static void setNomeCoordenadorES(String aNomeCoordenadorES) {
        nomeCoordenadorES = aNomeCoordenadorES;
    }

    /**
     * @return the generoCoordenadorES
     */
    public static String getGeneroCoordenadorES() {
        return generoCoordenadorES;
    }

    /**
     * @param genero the generoCoordenadorES to set
     */
    public static void setGeneroCoordenadorES(String genero) {
        if(genero.equals("a") || genero.equals("coordenadora")){
            generoCoordenadorES = "a";
        }else{
            generoCoordenadorES = "o";
        }
    }

    /**
     * @return the nomeCoordenadorCC
     */
    public static String getNomeCoordenadorCC() {
        return nomeCoordenadorCC;
    }

    /**
     * @param aNomeCoordenadorCC the nomeCoordenadorCC to set
     */
    public static void setNomeCoordenadorCC(String aNomeCoordenadorCC) {
        nomeCoordenadorCC = aNomeCoordenadorCC;
    }

    /**
     * @return the generoCoordenadorCC
     */
    public static String getGeneroCoordenadorCC() {
        return generoCoordenadorCC;
    }

    /**
     * @param genero the generoCoordenadorCC to set
     */
    public static void setGeneroCoordenadorCC(String genero) {
        if(genero.equals("a") || genero.equals("coordenadora")){
            generoCoordenadorCC = "a";
        }else{
            generoCoordenadorCC = "o";
        }
    }
    
}
