/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ultimatetcc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Jean Cheiran jfpcheiran@gmail.com
 */
public class Carregador {

    public static ArrayList<Aluno> carregarAlunos(String arquivoDeAlunos) throws RuntimeException, FileNotFoundException, IOException {
        ArrayList<Aluno> alunos = new ArrayList<>();
        File arquivo = new File(arquivoDeAlunos);
        FileReader preleitor = null;

        try {
            preleitor = new FileReader(arquivo);
        } catch (FileNotFoundException ex) {
            throw new FileNotFoundException("O arquivo CSV de alunos não pode ser encontrado.");
        }

        BufferedReader leitor = new BufferedReader(preleitor);

        String linha = null;
        int contadorDeLinhas = 1;
        do {

            try {
                linha = leitor.readLine();
            } catch (IOException ex) {
                throw new IOException("A linha " + contadorDeLinhas + " do arquivo CSV de alunos não pode ser lida.");
            }

            if (linha != null) {
                //System.out.println(linha);
                String[] dados = linha.split(",");
                if (dados.length != 3) {
                    throw new RuntimeException("Linha " + contadorDeLinhas + " do arquivo " + arquivoDeAlunos + " não contém dados de acordo com o esperado para alunos.");
                }
                alunos.add(new Aluno(dados[0], dados[1], dados[2].charAt(0)));
            }

            contadorDeLinhas++;
        } while (linha != null);
        return alunos;
    }

    public static ArrayList<Avaliador> carregarAvaliadores(String arquivoDeAvaliadores) throws RuntimeException, FileNotFoundException, IOException {
        ArrayList<Avaliador> avaliadores = new ArrayList<>();
        File arquivo = new File(arquivoDeAvaliadores);
        FileReader preleitor = null;
        
        try {
            preleitor = new FileReader(arquivo);
        } catch (FileNotFoundException ex) {
            throw new FileNotFoundException("O arquivo CSV de avaliadores não pode ser encontrado.");
        }

        BufferedReader leitor = new BufferedReader(preleitor);

        String linha = null;
        int contadorDeLinhas = 1;
        do {

            try {
                linha = leitor.readLine();
            } catch (IOException ex) {
                throw new IOException("A linha " + contadorDeLinhas + " do arquivo CSV de avaliadores não pode ser lida.");
            }

            if (linha != null) {
                //System.out.println(linha);
                String[] dados = linha.split(",");
                if (dados.length != 4) {
                    throw new RuntimeException("Linha " + contadorDeLinhas + " do arquivo " + arquivoDeAvaliadores + " não contém dados de acordo com o esperado para avaliadores.");
                }
                avaliadores.add(new Avaliador(dados[0], dados[1].charAt(0), dados[2], dados[3]));
            }

            contadorDeLinhas++;
        } while (linha != null);
        return avaliadores;
    }

    public static ArrayList<Banca> carregarBancas(String arquivoDeBancas, String arquivoDeAlunos, ArrayList<Aluno> alunos, String arquivoDeAvaliadores, ArrayList<Avaliador> avaliadores) throws RuntimeException, FileNotFoundException, IOException {
        ArrayList<Banca> bancas = new ArrayList<>();
        File arquivo = new File(arquivoDeBancas);
        FileReader preleitor = null;

        try {
            preleitor = new FileReader(arquivo);
        } catch (FileNotFoundException ex) {
            throw new FileNotFoundException("O arquivo CSV de bancas não pode ser encontrado.");
        }

        BufferedReader leitor = new BufferedReader(preleitor);

        String linha = null;
        int contadorDeLinhas = 1;
        do {

            try {
                linha = leitor.readLine();
            } catch (IOException ex) {
                throw new IOException("A linha " + contadorDeLinhas + " do arquivo CSV de bancas não pode ser lida.");
            }

            if (linha != null) {
                //System.out.println(linha);
                String[] dados = linha.split(",");
                if (dados.length < 12 || dados.length > 14) {
                    throw new RuntimeException("Linha " + contadorDeLinhas + " do arquivo " + arquivoDeBancas + " não contém dados de acordo com o esperado para bancas.");
                }
                Banca banca = null;

                int quantidadeDeAvaliadores = -1;
                //encontrando a posição da data para ver quantos avaliadores tem
                if (dados[7].split("/").length == 3) {
                    quantidadeDeAvaliadores = 2;
                } else if (dados[8].split("/").length == 3) {
                    quantidadeDeAvaliadores = 3;
                } else if (dados[9].split("/").length == 3) {
                    quantidadeDeAvaliadores = 4;
                }
                //se houve problema em achar a posição da data
                if (quantidadeDeAvaliadores == -1) {
                    throw new RuntimeException("Linha " + contadorDeLinhas + " do arquivo " + arquivoDeBancas + " não contém dados de acordo com o esperado.");
                }

                //aluno em 2
                //orientador em 3
                //coorientador em 4
                //avaliador 1 em 5
                //avaliador 2 em 6
                //outros avaliadores estão adiante
                banca = new Banca(dados[0], dados[1].equals("I"), dados[7 + (quantidadeDeAvaliadores - 2)], dados[8 + (quantidadeDeAvaliadores - 2)], dados[9 + (quantidadeDeAvaliadores - 2)], dados[10 + (quantidadeDeAvaliadores - 2)], dados[11 + (quantidadeDeAvaliadores - 2)]);

                //se o aluno está cadastrado
                for (Aluno a : alunos) {
                    if (a.getNome().equals(dados[2])) {
                        banca.setAluno(a);
                    }
                }
                if (banca.getAluno() == null) {
                    throw new RuntimeException("Linha " + contadorDeLinhas + " do arquivo " + arquivoDeBancas + " contém um aluno não cadastrado no arquivo " + arquivoDeAlunos + ".");
                }
                //se o orientador está cadastrado
                for (Avaliador a : avaliadores) {
                    if (a.getNome().equals(dados[3])) {
                        banca.setOrientador(a);
                    }
                }
                if (banca.getOrientador() == null) {
                    throw new RuntimeException("Linha " + contadorDeLinhas + " do arquivo " + arquivoDeBancas + " contém um orientador não cadastrado no arquivo " + arquivoDeAvaliadores + ".");
                }
                //se há coorientador e o coorientador está cadastrado
                if (!dados[4].equals("")) {
                    for (Avaliador a : avaliadores) {
                        if (a.getNome().equals(dados[4])) {
                            banca.setCoorientador(a);
                        }
                    }
                    if (banca.getCoorientador() == null) {
                        throw new RuntimeException("Linha " + contadorDeLinhas + " do arquivo " + arquivoDeBancas + " contém um coorientador não cadastrado no arquivo " + arquivoDeAvaliadores + ".");
                    }
                }
                //se os avaliadores da banca estão cadastrados
                for (int i = 0; i < quantidadeDeAvaliadores; i++) {
                    boolean achou = false;
                    for (Avaliador a : avaliadores) {
                        if (a.getNome().equals(dados[5 + i])) {
                            banca.addMembroDeBanca(a);
                            achou = true;
                        }
                    }
                    if (!achou) {
                        throw new RuntimeException("Linha " + contadorDeLinhas + " do arquivo " + arquivoDeBancas + " contém um avaliador não cadastrado no arquivo " + arquivoDeAvaliadores + ".");
                    }
                }

                bancas.add(banca);
            }

            contadorDeLinhas++;
        } while (linha != null);
        return bancas;
    }

}
