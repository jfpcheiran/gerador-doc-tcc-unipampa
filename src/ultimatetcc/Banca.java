/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ultimatetcc;

import java.util.ArrayList;

/**
 *
 * @author Jean Cheiran jfpcheiran@gmail.com
 */
public class Banca {
    
    private String curso;
    private boolean tcc1;
    private Aluno aluno;
    private Avaliador orientador;
    private Avaliador coorientador;
    private ArrayList<Avaliador> banca;
    private String data;
    private String horario;
    private String local;
    private String titulo;
    private String link;
    
    public Banca(String curso, boolean tcc1, String data, String horario, 
            String local, String titulo, String link){
        this.curso = curso;
        this.tcc1 = tcc1;
        this.aluno = null;
        this.orientador = null;
        this.coorientador = null;
        this.banca = new ArrayList<>();
        this.data = data;
        this.horario = horario;
        this.local = local;
        this.titulo = titulo;
        this.link = link;
    }

    /**
     * @return the curso
     */
    public String getCurso() {
        return curso;
    }

    /**
     * @param curso the curso to set
     */
    public void setCurso(String curso) {
        this.curso = curso;
    }

    /**
     * @return the tcc1
     */
    public boolean isTcc1() {
        return tcc1;
    }
    
    /**
     * @return the !tcc1
     */
    public boolean isTcc2() {
        return !tcc1;
    }

    /**
     * 
     */
    public void setTcc1() {
        this.tcc1 = true;
    }
    
    /**
     * 
     */
    public void setTcc2() {
        this.tcc1 = false;
    }

    /**
     * @return the aluno
     */
    public Aluno getAluno() {
        return aluno;
    }

    /**
     * @param aluno the aluno to set
     */
    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    /**
     * @return the orientador
     */
    public Avaliador getOrientador() {
        return orientador;
    }

    /**
     * @param orientador the orientador to set
     */
    public void setOrientador(Avaliador orientador) {
        this.orientador = orientador;
    }

    /**
     * @return the coorientador
     */
    public Avaliador getCoorientador() {
        return coorientador;
    }

    /**
     * @param coorientador the coorientador to set
     */
    public void setCoorientador(Avaliador coorientador) {
        this.coorientador = coorientador;
    }

    /**
     * @return the banca
     */
    public ArrayList<Avaliador> getBanca() {
        return banca;
    }

    /**
     * @param banca the banca to set
     */
    public void addMembroDeBanca(Avaliador avaliador) {
        this.banca.add(avaliador);
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the horario
     */
    public String getHorario() {
        return horario;
    }

    /**
     * @param horario the horario to set
     */
    public void setHorario(String horario) {
        this.horario = horario;
    }

    /**
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }
    
    @Override
    public boolean equals(Object o){
        if(o == null){
            return false;
        }
        if(getClass() != o.getClass()){
            return false;
        }
        Banca outra = (Banca) o;
        if(this.curso.equals(outra.getCurso()) && this.tcc1==outra.isTcc1() 
                && this.aluno.equals(outra.getAluno()) && this.orientador.equals(outra.getOrientador())
                && this.data.equals(outra.getData()) && this.horario.equals(outra.getHorario())
                && this.banca.equals(outra.getBanca()) && this.titulo.equals(outra.getTitulo())){
            return true;
        }else{
            return false;
        }
    }
    
    @Override
    public String toString(){
        return "<TCC"+(this.isTcc1()?"I":"II")+" "+this.curso+","+this.aluno+","+this.orientador+","+this.data+","+this.horario+","+this.titulo+">";
    }
    
}
