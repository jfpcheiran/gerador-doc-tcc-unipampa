/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ultimatetcc;

/**
 *
 * @author Jean Cheiran jfpcheiran@gmail.com
 */
public class Aluno {
    
    private String matricula;
    private String nome;
    private char genero;
    
    public Aluno(String matricula, String nome, char genero){
        this.matricula = matricula;
        this.nome = nome;
        switch(genero){
            case 'O':
            case 'm': 
            case 'M':
                this.genero = 'o';
                break;
            case 'A':
            case 'f': 
            case 'F':
                this.genero = 'a';
                break;
            default:
                this.genero = genero;
        }
    }

    /**
     * @return the matricula
     */
    public String getMatricula() {
        return matricula;
    }

    /**
     * @param matricula the matricula to set
     */
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the genero
     */
    public char getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(char genero) {
        switch(genero){
            case 'O':
            case 'm': 
            case 'M':
                this.genero = 'o';
                break;
            case 'A':
            case 'f': 
            case 'F':
                this.genero = 'a';
                break;
            default:
                this.genero = genero;
        }
    }
    
    @Override
    public boolean equals(Object o){
        if(o == null){
            return false;
        }
        if(getClass() != o.getClass()){
            return false;
        }
        Aluno outro = (Aluno) o;
        if(this.matricula.equals(outro.getMatricula()) && this.nome.equals(outro.getNome())){
            return true;
        }else{
            return false;
        }
    }
    
    @Override
    public String toString(){
        return "<"+this.matricula+","+this.nome+""+">";
    }
    
}
