/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ultimatetcc;

/**
 *
 * @author Jean Cheiran jfpcheiran@gmail.com
 */
public class Avaliador {
    
    private String nome;
    private char genero;
    private String pronomeDeTratamento;
    private String email;

    public Avaliador(String nome, char genero, String pronome, String email){
        this.nome = nome;
        this.genero = genero;
        this.pronomeDeTratamento = pronome;
        this.email = email;
    }
    
    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the genero
     */
    public char getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(char genero) {
        this.genero = genero;
    }

    /**
     * @return the pronomeDeTratamento
     */
    public String getPronomeDeTratamento() {
        return pronomeDeTratamento;
    }

    /**
     * @param pronomeDeTratamento the pronomeDeTratamento to set
     */
    public void setPronomeDeTratamento(String pronomeDeTratamento) {
        this.pronomeDeTratamento = pronomeDeTratamento;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    @Override
    public boolean equals(Object o){
        if(o == null){
            return false;
        }
        if(getClass() != o.getClass()){
            return false;
        }
        Avaliador outro = (Avaliador) o;
        if(this.nome.equals(outro.getNome())){
            return true;
        }else{
            return false;
        }
    }
    
    public String toString(){
        return "<"+this.pronomeDeTratamento+" "+this.nome+","+this.email+">";
    }
    
}
