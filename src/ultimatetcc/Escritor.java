/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ultimatetcc;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 *
 * @author Jean Cheiran jfpcheiran@gmail.com
 */
public class Escritor {

    public static void gerarPDF(String pastaPDF, ArrayList<Aluno> alunos, ArrayList<Avaliador> avaliadores, ArrayList<Banca> bancas) throws DocumentException, FileNotFoundException {
        Font titulo = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
        Font textoEmDestaque = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
        Font texto = new Font(Font.FontFamily.HELVETICA, 12);
        Font textinhoEmDestaque = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
        Font textinho = new Font(Font.FontFamily.HELVETICA, 10);
        Font textinhozinho = new Font(Font.FontFamily.HELVETICA, 8);

        for (Banca banca : bancas) {
            Document document = new Document();

            try {
                PdfWriter.getInstance(document, new FileOutputStream(pastaPDF + (pastaPDF.endsWith("/") ? "" : "/") + banca.getAluno().getMatricula() + ".pdf"));
            } catch (FileNotFoundException ex) {
                throw new FileNotFoundException("O arquivo " + (banca.getAluno().getMatricula() + ".pdf") + " não pode ser criado.");
            }
            document.open();

            document.addTitle("Documentos de banca de " + banca.getAluno().getNome());
            document.addAuthor("Jean Cheiran");
            document.addCreator("Jean Cheiran");

            //--------------------------------------------------------------
            //ata e parecer final
            Paragraph paragrafo, paragrafinho;
            paragrafo = new Paragraph("Universidade Federal do Pampa", titulo);
            paragrafo.setAlignment(Element.ALIGN_CENTER);
            document.add(paragrafo);
            paragrafo = new Paragraph("Campus Alegrete", titulo);
            paragrafo.setAlignment(Element.ALIGN_CENTER);
            document.add(paragrafo);
            if (banca.getCurso().equals("CC")) {
                paragrafo = new Paragraph("Curso de Ciência da Computação", titulo);
            } else {
                paragrafo = new Paragraph("Curso de Engenharia de Software", titulo);
            }
            paragrafo.setAlignment(Element.ALIGN_CENTER);
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            paragrafo = new Paragraph("ATA E PARECER FINAL DE DEFESA DE TRABALHO DE CONCLUSÃO DE CURSO " + (banca.isTcc1() ? "I" : "II"), titulo);
            paragrafo.setAlignment(Element.ALIGN_CENTER);
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            paragrafo = new Paragraph("Alun" + banca.getAluno().getGenero() + ": " + banca.getAluno().getNome() + "                    Matrícula: " + banca.getAluno().getMatricula(), texto);
            document.add(paragrafo);
            paragrafo = new Paragraph("Título do trabalho: " + banca.getTitulo(), texto);
            document.add(paragrafo);
            paragrafo = new Paragraph("Orientador" + (banca.getOrientador().getGenero() == 'a' ? "a" : "") + ": " + banca.getOrientador().getPronomeDeTratamento() + " " + banca.getOrientador().getNome(), texto);
            document.add(paragrafo);
            if (banca.getCoorientador() != null) {
                paragrafo = new Paragraph("Coorientador" + (banca.getCoorientador().getGenero() == 'a' ? "a" : "") + ": " + banca.getCoorientador().getPronomeDeTratamento() + " " + banca.getCoorientador().getNome(), texto);
                document.add(paragrafo);
            }
            paragrafo = new Paragraph("Data: " + banca.getData() + "                    Início: " + banca.getHorario() + "                    Local: " + banca.getLocal(), texto);
            document.add(paragrafo);
            document.add(new Paragraph(" "));

            paragrafo = new Paragraph();
            PdfPTable tabela = new PdfPTable(2);
            PdfPCell celula = new PdfPCell(new Phrase("Banca examinadora", textoEmDestaque));
            celula.setHorizontalAlignment(Element.ALIGN_CENTER);
            celula.setFixedHeight(22.0f);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase("Nota", textoEmDestaque));
            celula.setHorizontalAlignment(Element.ALIGN_CENTER);
            celula.setFixedHeight(22.0f);
            tabela.addCell(celula);
            tabela.setHeaderRows(1);
            celula = new PdfPCell(new Phrase(banca.getOrientador().getNome() + " (orientador" + (banca.getOrientador().getGenero() == 'a' ? "a" : "") + ")", texto));
            celula.setFixedHeight(22.0f);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(22.0f);
            tabela.addCell(celula);
            if (banca.getCoorientador() != null) {
                celula = new PdfPCell(new Phrase(banca.getCoorientador().getNome() + " (coorientador" + (banca.getCoorientador().getGenero() == 'a' ? "a" : "") + ")", texto));
                celula.setFixedHeight(22.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(22.0f);
                tabela.addCell(celula);
            }
            for (Avaliador a : banca.getBanca()) {
                celula = new PdfPCell(new Phrase(a.getNome(), texto));
                celula.setFixedHeight(22.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(22.0f);
                tabela.addCell(celula);
            }
            celula = new PdfPCell(new Phrase("Média final:", textoEmDestaque));
            celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celula.setFixedHeight(22.0f);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(22.0f);
            tabela.addCell(celula);
            tabela.setWidthPercentage(new float[]{0.75f, 0.25f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));
            paragrafo.add(tabela);
            paragrafo.setSpacingAfter(5.0f);
            document.add(paragrafo);
            //document.add(new Paragraph(" "));

            paragrafo = new Paragraph();
            tabela = new PdfPTable(2);
            celula = new PdfPCell(new Phrase("Situação d" + banca.getAluno().getGenero() + " alun" + banca.getAluno().getGenero() + ":", textoEmDestaque));
            celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celula.setVerticalAlignment(Element.ALIGN_MIDDLE);
            celula.setFixedHeight(50.0f);
            tabela.addCell(celula);
            celula = new PdfPCell();
            celula.addElement(new Paragraph("(   ) Aprovad" + banca.getAluno().getGenero(), texto));
            celula.addElement(new Paragraph("(   ) Reprovad" + banca.getAluno().getGenero(), texto));
            celula.setFixedHeight(50.0f);
            tabela.addCell(celula);
            tabela.setWidthPercentage(new float[]{0.75f, 0.25f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));
            paragrafo.add(tabela);
            paragrafo.setSpacingAfter(5.0f);
            document.add(paragrafo);
            //document.add(new Paragraph(" "));

            paragrafo = new Paragraph();
            tabela = new PdfPTable(2);
            celula = new PdfPCell(new Phrase("Considerações:", textoEmDestaque));
            celula.setHorizontalAlignment(Element.ALIGN_LEFT);
            celula.setVerticalAlignment(Element.ALIGN_MIDDLE);
            celula.setFixedHeight(100.0f);
            tabela.addCell(celula);
            celula = new PdfPCell();
            celula.addElement(new Phrase(" "));
            celula.setFixedHeight(100.0f);
            tabela.addCell(celula);
            tabela.setWidthPercentage(new float[]{0.2f, 0.8f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));
            paragrafo.add(tabela);
            document.add(paragrafo);
            //document.add(new Paragraph(" "));

            paragrafo = new Paragraph(Escritor.getLocalData(banca.getData()), texto);
            paragrafo.setAlignment(Element.ALIGN_RIGHT);
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            //document.add(new Paragraph(" "));

            if (banca.getCoorientador() == null) {
                paragrafo = new Paragraph("___________________________________", texto);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph(banca.getOrientador().getPronomeDeTratamento() + " " + banca.getOrientador().getNome(), textoEmDestaque);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph("Presidente da banca examinadora", texto);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
            } else {
                paragrafo = new Paragraph();
                tabela = new PdfPTable(2);
                celula = new PdfPCell(new Phrase("___________________________________", texto));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                celula.setBorder(0);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("___________________________________", texto));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                celula.setBorder(0);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(banca.getOrientador().getPronomeDeTratamento() + " " + banca.getOrientador().getNome(), textoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                celula.setBorder(0);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(banca.getCoorientador().getPronomeDeTratamento() + " " + banca.getCoorientador().getNome(), textoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                celula.setBorder(0);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("Presidente da banca examinadora", texto));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                celula.setBorder(0);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("Examinador" + (banca.getCoorientador().getGenero() == 'a' ? "a" : ""), texto));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                celula.setBorder(0);
                tabela.addCell(celula);
                paragrafo.add(tabela);
                tabela.setWidthPercentage(new float[]{0.5f, 0.5f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));
                document.add(paragrafo);
            }
            document.add(new Paragraph(" "));

            if (banca.getCoorientador() == null) {
                paragrafo = new Paragraph("___________________________________", texto);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph(banca.getBanca().get(0).getPronomeDeTratamento() + " " + banca.getBanca().get(0).getNome(), textoEmDestaque);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph("Examinador" + (banca.getBanca().get(0).getGenero() == 'a' ? "a" : ""), texto);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));

                paragrafo = new Paragraph("___________________________________", texto);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph(banca.getBanca().get(1).getPronomeDeTratamento() + " " + banca.getBanca().get(1).getNome(), textoEmDestaque);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph("Examinador" + (banca.getBanca().get(1).getGenero() == 'a' ? "a" : ""), texto);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
            } else {
                paragrafo = new Paragraph();
                tabela = new PdfPTable(2);
                celula = new PdfPCell(new Phrase("___________________________________", texto));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                celula.setBorder(0);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("___________________________________", texto));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                celula.setBorder(0);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(banca.getBanca().get(0).getPronomeDeTratamento() + " " + banca.getBanca().get(0).getNome(), textoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                celula.setBorder(0);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(banca.getBanca().get(1).getPronomeDeTratamento() + " " + banca.getBanca().get(1).getNome(), textoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                celula.setBorder(0);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("Examinador" + (banca.getBanca().get(0).getGenero() == 'a' ? "a" : ""), texto));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                celula.setBorder(0);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("Examinador" + (banca.getBanca().get(1).getGenero() == 'a' ? "a" : ""), texto));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                celula.setBorder(0);
                tabela.addCell(celula);
                paragrafo.add(tabela);
                tabela.setWidthPercentage(new float[]{0.5f, 0.5f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));
                document.add(paragrafo);
            }

            document.newPage();
            document.add(new Paragraph(" "));

            //se ainda houver avaliadores, vão para trás da folha
            switch (banca.getBanca().size()) {
                case 3:
                    paragrafo = new Paragraph("___________________________________", texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    paragrafo = new Paragraph(banca.getBanca().get(2).getPronomeDeTratamento() + " " + banca.getBanca().get(2).getNome(), textoEmDestaque);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    paragrafo = new Paragraph("Examinador" + (banca.getBanca().get(2).getGenero() == 'a' ? "a" : ""), texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    break;
                case 4:
                    paragrafo = new Paragraph();
                    tabela = new PdfPTable(2);
                    celula = new PdfPCell(new Phrase("___________________________________", texto));
                    celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                    celula.setFixedHeight(22.0f);
                    celula.setBorder(0);
                    tabela.addCell(celula);
                    celula = new PdfPCell(new Phrase("___________________________________", texto));
                    celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                    celula.setFixedHeight(22.0f);
                    celula.setBorder(0);
                    tabela.addCell(celula);
                    celula = new PdfPCell(new Phrase(banca.getBanca().get(2).getPronomeDeTratamento() + " " + banca.getBanca().get(2).getNome(), textoEmDestaque));
                    celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                    celula.setFixedHeight(22.0f);
                    celula.setBorder(0);
                    tabela.addCell(celula);
                    celula = new PdfPCell(new Phrase(banca.getBanca().get(3).getPronomeDeTratamento() + " " + banca.getBanca().get(3).getNome(), textoEmDestaque));
                    celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                    celula.setFixedHeight(22.0f);
                    celula.setBorder(0);
                    tabela.addCell(celula);
                    celula = new PdfPCell(new Phrase("Examinador" + (banca.getBanca().get(2).getGenero() == 'a' ? "a" : ""), texto));
                    celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                    celula.setFixedHeight(22.0f);
                    celula.setBorder(0);
                    tabela.addCell(celula);
                    celula = new PdfPCell(new Phrase("Examinador" + (banca.getBanca().get(3).getGenero() == 'a' ? "a" : ""), texto));
                    celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                    celula.setFixedHeight(22.0f);
                    celula.setBorder(0);
                    tabela.addCell(celula);
                    paragrafo.add(tabela);
                    tabela.setWidthPercentage(new float[]{0.5f, 0.5f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));
                    document.add(paragrafo);
                    break;
            }
            document.newPage();

            //--------------------------------------------------------------
            //lista de presença
            for (int i = 0; i < 4; i++) {
                paragrafo = new Paragraph("Universidade Federal do Pampa", titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph("Campus Alegrete", titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                if (banca.getCurso().equals("CC")) {
                    paragrafo = new Paragraph("Curso de Ciência da Computação", titulo);
                } else {
                    paragrafo = new Paragraph("Curso de Engenharia de Software", titulo);
                }
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("LISTA DE PRESENÇA", titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("Disciplina: Trabalho de Conclusão de Curso " + (banca.isTcc1() ? "I" : "II"), texto);
                document.add(paragrafo);
                paragrafo = new Paragraph("Coordenador" + (Configuracoes.getGeneroCoordenadorTCC().equals("a") ? "a" : "") + " de TCC: " + Configuracoes.getNomeCoordenadorTCC(), texto);
                document.add(paragrafo);
                paragrafo = new Paragraph("Data: " + banca.getData(), texto);
                document.add(paragrafo);
                paragrafo = new Paragraph("Horário: " + banca.getHorario(), texto);
                document.add(paragrafo);
                paragrafo = new Paragraph("Apresentador" + (banca.getAluno().getGenero() == 'a' ? "a" : "") + ": " + banca.getAluno().getNome(), texto);
                document.add(paragrafo);
                document.add(new Paragraph(" "));

                paragrafo = new Paragraph();
                tabela = new PdfPTable(3);
                celula = new PdfPCell(new Phrase("Matrícula", textoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("Nome", textoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("Assinatura", textoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(22.0f);
                tabela.addCell(celula);
                tabela.setHeaderRows(1);
                for (int j = 0; j < 20; j++) {
                    celula = new PdfPCell(new Phrase("", texto));
                    celula.setFixedHeight(25.0f);
                    tabela.addCell(celula);
                    celula = new PdfPCell(new Phrase("", texto));
                    celula.setFixedHeight(25.0f);
                    tabela.addCell(celula);
                    celula = new PdfPCell(new Phrase("", texto));
                    celula.setFixedHeight(25.0f);
                    tabela.addCell(celula);
                }
                tabela.setWidthPercentage(new float[]{0.20f, 0.4f, 0.4f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));
                paragrafo.add(tabela);
                document.add(paragrafo);
                document.newPage();
            }

            //--------------------------------------------------------------
            //formulário do orientador
            paragrafo = new Paragraph("Universidade Federal do Pampa", titulo);
            paragrafo.setAlignment(Element.ALIGN_CENTER);
            document.add(paragrafo);
            paragrafo = new Paragraph("Campus Alegrete", titulo);
            paragrafo.setAlignment(Element.ALIGN_CENTER);
            document.add(paragrafo);
            if (banca.getCurso().equals("CC")) {
                paragrafo = new Paragraph("Curso de Ciência da Computação", titulo);
            } else {
                paragrafo = new Paragraph("Curso de Engenharia de Software", titulo);
            }
            paragrafo.setAlignment(Element.ALIGN_CENTER);
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            paragrafo = new Paragraph("FORMULÁRIO DE AVALIAÇÃO DE TRABALHO DE CONCLUSÃO DE CURSO " + (banca.isTcc1() ? "I" : "II"), titulo);
            paragrafo.setAlignment(Element.ALIGN_CENTER);
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            paragrafo = new Paragraph("Alun" + banca.getAluno().getGenero() + ": " + banca.getAluno().getNome() + "                    Matrícula: " + banca.getAluno().getMatricula(), textinho);
            document.add(paragrafo);
            paragrafo = new Paragraph("Título do trabalho: " + banca.getTitulo(), textinho);
            document.add(paragrafo);
            paragrafo = new Paragraph("Data: " + banca.getData() + "                    Início: " + banca.getHorario() + "                    Local: " + banca.getLocal(), textinho);
            document.add(paragrafo);
            paragrafo = new Paragraph("Avaliador: " + banca.getOrientador().getNome(), textinhoEmDestaque);
            document.add(paragrafo);
            document.add(new Paragraph(" "));

            paragrafo = new Paragraph();
            tabela = new PdfPTable(2);
            celula = new PdfPCell(new Phrase("I – TRABALHO DESENVOLVIDO (4,0)", textinhoEmDestaque));
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase("Nota", textinhoEmDestaque));
            celula.setHorizontalAlignment(Element.ALIGN_CENTER);
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            tabela.setHeaderRows(1);

            celula = new PdfPCell();
            celula.setFixedHeight(36.0f);
            paragrafinho = new Paragraph("Relevância (1,0)", textinhoEmDestaque);
            celula.addElement(paragrafinho);
            paragrafinho = new Paragraph("O problema é claramente apresentado e devidamente justificado.", textinho);
            celula.addElement(paragrafinho);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(36.0f);
            tabela.addCell(celula);

            celula = new PdfPCell();
            celula.setFixedHeight(36.0f);
            paragrafinho = new Paragraph("Estado da Arte (1,0)", textinhoEmDestaque);
            celula.addElement(paragrafinho);
            paragrafinho = new Paragraph("O referêncial teórico está adequado e atualizado.", textinho);
            celula.addElement(paragrafinho);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(36.0f);
            tabela.addCell(celula);

            celula = new PdfPCell();
            celula.setFixedHeight(36.0f);
            paragrafinho = new Paragraph("Corretude técnica (1,0)", textinhoEmDestaque);
            celula.addElement(paragrafinho);
            if (banca.isTcc1()) {
                paragrafinho = new Paragraph("A metodologia proposta é adequada ao tratamento do problema.", textinho);
            } else {
                paragrafinho = new Paragraph("O problema foi resolvido utilizando a metodologia adequada.", textinho);
            }
            celula.addElement(paragrafinho);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(36.0f);
            tabela.addCell(celula);

            celula = new PdfPCell();
            celula.setFixedHeight(36.0f);
            paragrafinho = new Paragraph("Abrangência (1,0)", textinhoEmDestaque);
            celula.addElement(paragrafinho);
            if (banca.isTcc1()) {
                paragrafinho = new Paragraph("As atividades previstas e o cronograma são viáveis.", textinho);
            } else {
                paragrafinho = new Paragraph("O trabalho está completo e os seus objetivos específicos foram atingidos.", textinho);
            }
            celula.addElement(paragrafinho);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(36.0f);
            tabela.addCell(celula);
            tabela.setWidthPercentage(new float[]{0.85f, 0.15f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));

            celula = new PdfPCell(new Phrase("Nota parcial:", textinhoEmDestaque));
            celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            paragrafo.add(tabela);
            paragrafo.setSpacingAfter(5.0f);
            document.add(paragrafo);

            paragrafo = new Paragraph();
            tabela = new PdfPTable(2);
            celula = new PdfPCell(new Phrase("II – APRESENTAÇÃO (3,0)", textinhoEmDestaque));
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase("Nota", textinhoEmDestaque));
            celula.setHorizontalAlignment(Element.ALIGN_CENTER);
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            tabela.setHeaderRows(1);

            celula = new PdfPCell();
            celula.setFixedHeight(36.0f);
            paragrafinho = new Paragraph("Clareza (1,0)", textinhoEmDestaque);
            celula.addElement(paragrafinho);
            paragrafinho = new Paragraph("As ideias foram expostas de maneira crítica e em consonância com o texto.", textinho);
            celula.addElement(paragrafinho);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(36.0f);
            tabela.addCell(celula);

            celula = new PdfPCell();
            celula.setFixedHeight(36.0f);
            paragrafinho = new Paragraph("Conhecimento (1,0)", textinhoEmDestaque);
            celula.addElement(paragrafinho);
            paragrafinho = new Paragraph("Na abordagem do tema foi demonstrado segurança e domínio do assunto.", textinho);
            celula.addElement(paragrafinho);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(36.0f);
            tabela.addCell(celula);

            celula = new PdfPCell();
            celula.setFixedHeight(36.0f);
            paragrafinho = new Paragraph("Planejamento (1,0)", textinhoEmDestaque);
            celula.addElement(paragrafinho);
            paragrafinho = new Paragraph("A apresentação teve sequência lógica e divisão equitativa do tempo.", textinho);
            celula.addElement(paragrafinho);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(36.0f);
            tabela.addCell(celula);
            tabela.setWidthPercentage(new float[]{0.85f, 0.15f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));

            celula = new PdfPCell(new Phrase("Nota parcial:", textinhoEmDestaque));
            celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            paragrafo.add(tabela);
            paragrafo.setSpacingAfter(5.0f);
            document.add(paragrafo);

            paragrafo = new Paragraph();
            tabela = new PdfPTable(2);
            celula = new PdfPCell(new Phrase("III – QUALIDADE DO TEXTO (3,0)", textinhoEmDestaque));
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase("Nota", textinhoEmDestaque));
            celula.setHorizontalAlignment(Element.ALIGN_CENTER);
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            tabela.setHeaderRows(1);

            celula = new PdfPCell();
            celula.setFixedHeight(36.0f);
            paragrafinho = new Paragraph("Clareza (1,0)", textinhoEmDestaque);
            celula.addElement(paragrafinho);
            paragrafinho = new Paragraph("As ideias foram expostas de maneira clara e o texto é legível.", textinho);
            celula.addElement(paragrafinho);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(36.0f);
            tabela.addCell(celula);

            celula = new PdfPCell();
            celula.setFixedHeight(36.0f);
            paragrafinho = new Paragraph("Gramática e Ortografia (1,0)", textinhoEmDestaque);
            celula.addElement(paragrafinho);
            paragrafinho = new Paragraph("O texto segue as regras gramaticais e ortográficas da língua portuguesa.", textinho);
            celula.addElement(paragrafinho);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(36.0f);
            tabela.addCell(celula);

            celula = new PdfPCell();
            celula.setFixedHeight(36.0f);
            paragrafinho = new Paragraph("Estrutura e Organização (1,0)", textinhoEmDestaque);
            celula.addElement(paragrafinho);
            paragrafinho = new Paragraph("O texto está bem estruturado e organizado.", textinho);
            celula.addElement(paragrafinho);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(36.0f);
            tabela.addCell(celula);
            tabela.setWidthPercentage(new float[]{0.85f, 0.15f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));

            celula = new PdfPCell(new Phrase("Notal parcial:", textinhoEmDestaque));
            celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            paragrafo.add(tabela);
            paragrafo.setSpacingAfter(5.0f);
            document.add(paragrafo);

            paragrafo = new Paragraph();
            tabela = new PdfPTable(2);
            celula = new PdfPCell(new Phrase("NOTA FINAL:", textinhoEmDestaque));
            celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            celula = new PdfPCell(new Phrase(" "));
            celula.setFixedHeight(17.0f);
            tabela.addCell(celula);
            tabela.setWidthPercentage(new float[]{0.85f, 0.15f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));
            paragrafo.add(tabela);
            paragrafo.setSpacingAfter(5.0f);
            document.add(paragrafo);

            paragrafo = new Paragraph(Escritor.getLocalData(banca.getData()), textinho);
            document.add(paragrafo);

            paragrafo = new Paragraph("___________________________________", textinho);
            paragrafo.setAlignment(Element.ALIGN_RIGHT);
            document.add(paragrafo);
            paragrafo = new Paragraph(banca.getOrientador().getPronomeDeTratamento() + " " + banca.getOrientador().getNome(), textinhoEmDestaque);
            paragrafo.setAlignment(Element.ALIGN_RIGHT);
            document.add(paragrafo);

            paragrafo = new Paragraph("OBS.: comentários no verso.", textinhozinho);
            document.add(paragrafo);

            document.newPage();

            paragrafo = new Paragraph("COMENTÁRIOS", textoEmDestaque);
            paragrafo.setAlignment(Element.ALIGN_CENTER);
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            document.add(new Paragraph(" "));

            for (int i = 0; i < 25; i++) {
                paragrafo = new Paragraph("________________________________________________________________", texto);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                paragrafo.setSpacingAfter(10.0f);
                document.add(paragrafo);
            }
            document.newPage();

            //formulário do coorientador
            if (banca.getCoorientador() != null) {
                paragrafo = new Paragraph("Universidade Federal do Pampa", titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph("Campus Alegrete", titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                if (banca.getCurso().equals("CC")) {
                    paragrafo = new Paragraph("Curso de Ciência da Computação", titulo);
                } else {
                    paragrafo = new Paragraph("Curso de Engenharia de Software", titulo);
                }
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("FORMULÁRIO DE AVALIAÇÃO DE TRABALHO DE CONCLUSÃO DE CURSO " + (banca.isTcc1() ? "I" : "II"), titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("Alun" + banca.getAluno().getGenero() + ": " + banca.getAluno().getNome() + "                    Matrícula: " + banca.getAluno().getMatricula(), textinho);
                document.add(paragrafo);
                paragrafo = new Paragraph("Título do trabalho: " + banca.getTitulo(), textinho);
                document.add(paragrafo);
                paragrafo = new Paragraph("Data: " + banca.getData() + "                    Início: " + banca.getHorario() + "                    Local: " + banca.getLocal(), textinho);
                document.add(paragrafo);
                paragrafo = new Paragraph("Avaliador: " + banca.getCoorientador().getNome(), textinhoEmDestaque);
                document.add(paragrafo);
                document.add(new Paragraph(" "));

                paragrafo = new Paragraph();
                tabela = new PdfPTable(2);
                celula = new PdfPCell(new Phrase("I – TRABALHO DESENVOLVIDO (4,0)", textinhoEmDestaque));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("Nota", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                tabela.setHeaderRows(1);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Relevância (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("O problema é claramente apresentado e devidamente justificado.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Estado da Arte (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("O referêncial teórico está adequado e atualizado.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Corretude técnica (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                if (banca.isTcc1()) {
                    paragrafinho = new Paragraph("A metodologia proposta é adequada ao tratamento do problema.", textinho);
                } else {
                    paragrafinho = new Paragraph("O problema foi resolvido utilizando a metodologia adequada.", textinho);
                }
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Abrangência (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                if (banca.isTcc1()) {
                    paragrafinho = new Paragraph("As atividades previstas e o cronograma são viáveis.", textinho);
                } else {
                    paragrafinho = new Paragraph("O trabalho está completo e os seus objetivos específicos foram atingidos.", textinho);
                }
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);
                tabela.setWidthPercentage(new float[]{0.85f, 0.15f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));

                celula = new PdfPCell(new Phrase("Nota parcial:", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                paragrafo.add(tabela);
                paragrafo.setSpacingAfter(5.0f);
                document.add(paragrafo);

                paragrafo = new Paragraph();
                tabela = new PdfPTable(2);
                celula = new PdfPCell(new Phrase("II – APRESENTAÇÃO (3,0)", textinhoEmDestaque));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("Nota", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                tabela.setHeaderRows(1);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Clareza (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("As ideias foram expostas de maneira crítica e em consonância com o texto.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Conhecimento (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("Na abordagem do tema foi demonstrado segurança e domínio do assunto.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Planejamento (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("A apresentação teve sequência lógica e divisão equitativa do tempo.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);
                tabela.setWidthPercentage(new float[]{0.85f, 0.15f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));

                celula = new PdfPCell(new Phrase("Nota parcial:", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                paragrafo.add(tabela);
                paragrafo.setSpacingAfter(5.0f);
                document.add(paragrafo);

                paragrafo = new Paragraph();
                tabela = new PdfPTable(2);
                celula = new PdfPCell(new Phrase("III – QUALIDADE DO TEXTO (3,0)", textinhoEmDestaque));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("Nota", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                tabela.setHeaderRows(1);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Clareza (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("As ideias foram expostas de maneira clara e o texto é legível.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Gramática e Ortografia (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("O texto segue as regras gramaticais e ortográficas da língua portuguesa.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Estrutura e Organização (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("O texto está bem estruturado e organizado.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);
                tabela.setWidthPercentage(new float[]{0.85f, 0.15f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));

                celula = new PdfPCell(new Phrase("Notal parcial:", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                paragrafo.add(tabela);
                paragrafo.setSpacingAfter(5.0f);
                document.add(paragrafo);

                paragrafo = new Paragraph();
                tabela = new PdfPTable(2);
                celula = new PdfPCell(new Phrase("NOTA FINAL:", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                tabela.setWidthPercentage(new float[]{0.85f, 0.15f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));
                paragrafo.add(tabela);
                paragrafo.setSpacingAfter(5.0f);
                document.add(paragrafo);

                paragrafo = new Paragraph(Escritor.getLocalData(banca.getData()), textinho);
                document.add(paragrafo);

                paragrafo = new Paragraph("___________________________________", textinho);
                paragrafo.setAlignment(Element.ALIGN_RIGHT);
                document.add(paragrafo);
                paragrafo = new Paragraph(banca.getCoorientador().getPronomeDeTratamento() + " " + banca.getCoorientador().getNome(), textinhoEmDestaque);
                paragrafo.setAlignment(Element.ALIGN_RIGHT);
                document.add(paragrafo);

                paragrafo = new Paragraph("OBS.: comentários no verso.", textinhozinho);
                document.add(paragrafo);

                document.newPage();

                paragrafo = new Paragraph("COMENTÁRIOS", textoEmDestaque);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));

                for (int i = 0; i < 25; i++) {
                    paragrafo = new Paragraph("________________________________________________________________", texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    paragrafo.setSpacingAfter(10.0f);
                    document.add(paragrafo);
                }
                document.newPage();
            }

            //formulário dos demais avaliadores
            for (int i = 0; i < banca.getBanca().size(); i++) {
                paragrafo = new Paragraph("Universidade Federal do Pampa", titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph("Campus Alegrete", titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                if (banca.getCurso().equals("CC")) {
                    paragrafo = new Paragraph("Curso de Ciência da Computação", titulo);
                } else {
                    paragrafo = new Paragraph("Curso de Engenharia de Software", titulo);
                }
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("FORMULÁRIO DE AVALIAÇÃO DE TRABALHO DE CONCLUSÃO DE CURSO " + (banca.isTcc1() ? "I" : "II"), titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("Alun" + banca.getAluno().getGenero() + ": " + banca.getAluno().getNome() + "                    Matrícula: " + banca.getAluno().getMatricula(), textinho);
                document.add(paragrafo);
                paragrafo = new Paragraph("Título do trabalho: " + banca.getTitulo(), textinho);
                document.add(paragrafo);
                paragrafo = new Paragraph("Data: " + banca.getData() + "                    Início: " + banca.getHorario() + "                    Local: " + banca.getLocal(), textinho);
                document.add(paragrafo);
                paragrafo = new Paragraph("Avaliador: " + banca.getBanca().get(i).getNome(), textinhoEmDestaque);
                document.add(paragrafo);
                document.add(new Paragraph(" "));

                paragrafo = new Paragraph();
                tabela = new PdfPTable(2);
                celula = new PdfPCell(new Phrase("I – TRABALHO DESENVOLVIDO (4,0)", textinhoEmDestaque));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("Nota", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                tabela.setHeaderRows(1);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Relevância (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("O problema é claramente apresentado e devidamente justificado.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Estado da Arte (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("O referêncial teórico está adequado e atualizado.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Corretude técnica (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                if (banca.isTcc1()) {
                    paragrafinho = new Paragraph("A metodologia proposta é adequada ao tratamento do problema.", textinho);
                } else {
                    paragrafinho = new Paragraph("O problema foi resolvido utilizando a metodologia adequada.", textinho);
                }
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Abrangência (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                if (banca.isTcc1()) {
                    paragrafinho = new Paragraph("As atividades previstas e o cronograma são viáveis.", textinho);
                } else {
                    paragrafinho = new Paragraph("O trabalho está completo e os seus objetivos específicos foram atingidos.", textinho);
                }
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);
                tabela.setWidthPercentage(new float[]{0.85f, 0.15f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));

                celula = new PdfPCell(new Phrase("Nota parcial:", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                paragrafo.add(tabela);
                paragrafo.setSpacingAfter(5.0f);
                document.add(paragrafo);

                paragrafo = new Paragraph();
                tabela = new PdfPTable(2);
                celula = new PdfPCell(new Phrase("II – APRESENTAÇÃO (3,0)", textinhoEmDestaque));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("Nota", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                tabela.setHeaderRows(1);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Clareza (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("As ideias foram expostas de maneira crítica e em consonância com o texto.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Conhecimento (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("Na abordagem do tema foi demonstrado segurança e domínio do assunto.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Planejamento (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("A apresentação teve sequência lógica e divisão equitativa do tempo.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);
                tabela.setWidthPercentage(new float[]{0.85f, 0.15f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));

                celula = new PdfPCell(new Phrase("Nota parcial:", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                paragrafo.add(tabela);
                paragrafo.setSpacingAfter(5.0f);
                document.add(paragrafo);

                paragrafo = new Paragraph();
                tabela = new PdfPTable(2);
                celula = new PdfPCell(new Phrase("III – QUALIDADE DO TEXTO (3,0)", textinhoEmDestaque));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase("Nota", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_CENTER);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                tabela.setHeaderRows(1);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Clareza (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("As ideias foram expostas de maneira clara e o texto é legível.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Gramática e Ortografia (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("O texto segue as regras gramaticais e ortográficas da língua portuguesa.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);

                celula = new PdfPCell();
                celula.setFixedHeight(36.0f);
                paragrafinho = new Paragraph("Estrutura e Organização (1,0)", textinhoEmDestaque);
                celula.addElement(paragrafinho);
                paragrafinho = new Paragraph("O texto está bem estruturado e organizado.", textinho);
                celula.addElement(paragrafinho);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(36.0f);
                tabela.addCell(celula);
                tabela.setWidthPercentage(new float[]{0.85f, 0.15f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));

                celula = new PdfPCell(new Phrase("Notal parcial:", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                paragrafo.add(tabela);
                paragrafo.setSpacingAfter(5.0f);
                document.add(paragrafo);

                paragrafo = new Paragraph();
                tabela = new PdfPTable(2);
                celula = new PdfPCell(new Phrase("NOTA FINAL:", textinhoEmDestaque));
                celula.setHorizontalAlignment(Element.ALIGN_RIGHT);
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                celula = new PdfPCell(new Phrase(" "));
                celula.setFixedHeight(17.0f);
                tabela.addCell(celula);
                tabela.setWidthPercentage(new float[]{0.85f, 0.15f}, new Rectangle(0.0f, 0.0f, 1.0f, 1.0f));
                paragrafo.add(tabela);
                paragrafo.setSpacingAfter(5.0f);
                document.add(paragrafo);

                paragrafo = new Paragraph(Escritor.getLocalData(banca.getData()), textinho);
                document.add(paragrafo);

                paragrafo = new Paragraph("___________________________________", textinho);
                paragrafo.setAlignment(Element.ALIGN_RIGHT);
                document.add(paragrafo);
                paragrafo = new Paragraph(banca.getBanca().get(i).getPronomeDeTratamento() + " " + banca.getBanca().get(i).getNome(), textinhoEmDestaque);
                paragrafo.setAlignment(Element.ALIGN_RIGHT);
                document.add(paragrafo);

                paragrafo = new Paragraph("OBS.: comentários no verso.", textinhozinho);
                document.add(paragrafo);

                document.newPage();

                paragrafo = new Paragraph("COMENTÁRIOS", textoEmDestaque);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));

                for (int j = 0; j < 25; j++) {
                    paragrafo = new Paragraph("________________________________________________________________", texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    paragrafo.setSpacingAfter(10.0f);
                    document.add(paragrafo);
                }
                document.newPage();
            }

            //--------------------------------------------------------------
            //parecer final do orientador (para TCC2)
            if (banca.isTcc2()) {
                paragrafo = new Paragraph("Universidade Federal do Pampa", titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph("Campus Alegrete", titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                if (banca.getCurso().equals("CC")) {
                    paragrafo = new Paragraph("Curso de Ciência da Computação", titulo);
                } else {
                    paragrafo = new Paragraph("Curso de Engenharia de Software", titulo);
                }
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("PARECER FINAL DO ORIENTADOR", titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("    Eu, " + banca.getOrientador().getNome() + ", membro do corpo docente do Curso de "
                        + (banca.getCurso().equals("CC") ? "Ciência da Computação" : "Engenharia de Software") + ", declaro que "
                        + "li e estou de acordo com a versão final do Trabalho de Conclusão de Curso (TCC) intitulado", texto);
                paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph(banca.getTitulo(), titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("elaborado pel" + banca.getAluno().getGenero() + " acadêmic" + banca.getAluno().getGenero()
                        + " " + banca.getAluno().getNome() + ", matrícula " + banca.getAluno().getMatricula() + ". Todas as sugestões "
                        + "da banca examinadora foram avaliadas e executadas.", texto);
                paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("Alegrete, _____ de _________________ de ________", texto);
                paragrafo.setAlignment(Element.ALIGN_RIGHT);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("___________________________________", texto);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph(banca.getOrientador().getPronomeDeTratamento() + " " + banca.getOrientador().getNome(), textoEmDestaque);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);

                document.newPage();
                document.add(new Paragraph(" "));
                document.newPage();
            }
            //--------------------------------------------------------------
            //atestados (para TCC II)
            if (banca.isTcc2()) {
                //atestado orientador
                paragrafo = new Paragraph("Universidade Federal do Pampa", titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph("Campus Alegrete", titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                if (banca.getCurso().equals("CC")) {
                    paragrafo = new Paragraph("Curso de Ciência da Computação", titulo);
                } else {
                    paragrafo = new Paragraph("Curso de Engenharia de Software", titulo);
                }
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("ATESTADO", titulo);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph();
                paragrafo.add(new Phrase("    A coordenação do curso de "
                        + (banca.getCurso().equals("CC") ? "Ciência da Computação" : "Engenharia de Software")
                        + " da Universidade Federal do Pampa atesta, para os devidos fins, que ", texto));
                paragrafo.add(new Phrase(banca.getOrientador().getNome(), textoEmDestaque));
                paragrafo.add(new Phrase(" participou como orientador" + (banca.getOrientador().getGenero() == 'a' ? "a" : "")
                        + " da monografia de Trabalho de Conclusão de Curso d" + banca.getAluno().getGenero()
                        + " alun" + banca.getAluno().getGenero() + " ", texto));
                paragrafo.add(new Phrase(banca.getAluno().getNome(), textoEmDestaque));
                paragrafo.add(new Phrase(" intitulada \"", texto));
                paragrafo.add(new Phrase(banca.getTitulo(), textoEmDestaque));
                paragrafo.add(new Phrase("\" e apresentada no dia " + banca.getData() + ", no Campus Alegrete – "
                        + "UNIPAMPA, para a seguinte banca avaliadora: ", texto));
                paragrafo.add(new Phrase(banca.getOrientador().getNome() + ", ", texto));
                if (banca.getCoorientador() != null) {
                    paragrafo.add(new Phrase(banca.getCoorientador().getNome() + ", ", texto));
                }
                for (int i = 0; i < banca.getBanca().size() - 1; i++) {
                    paragrafo.add(new Phrase(banca.getBanca().get(i).getNome() + ", ", texto));
                }
                paragrafo.add(new Phrase(banca.getBanca().get(banca.getBanca().size() - 1).getNome() + ".", texto));
                paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph(Escritor.getLocalData(banca.getData()), texto);
                paragrafo.setAlignment(Element.ALIGN_RIGHT);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("___________________________________", texto);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph("Prof" + (Configuracoes.getGeneroCoordenadorTCC().equals("a") ? "a. " : ". ") + Configuracoes.getNomeCoordenadorTCC(), texto);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                paragrafo = new Paragraph("Coordenador" + (Configuracoes.getGeneroCoordenadorTCC().equals("a") ? "a" : "") + " de Trabalho de Conclusão de Curso", texto);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));
                paragrafo = new Paragraph("___________________________________", texto);
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);
                if (banca.getCurso().equals("CC")) {
                    paragrafo = new Paragraph("Prof" + (Configuracoes.getGeneroCoordenadorCC().equals("a") ? "a. " : ". ") + Configuracoes.getNomeCoordenadorCC(), texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    paragrafo = new Paragraph("Coordenador" + (Configuracoes.getGeneroCoordenadorCC().equals("a") ? "a" : "") + " do Curso de Ciência da Computação", texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                } else {
                    paragrafo = new Paragraph("Prof" + (Configuracoes.getGeneroCoordenadorES().equals("a") ? "a. " : ". ") + Configuracoes.getNomeCoordenadorES(), texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    paragrafo = new Paragraph("Coordenador" + (Configuracoes.getGeneroCoordenadorES().equals("a") ? "a" : "") + " do Curso de Engenharia de Software", texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                }

                document.newPage();
                document.add(new Paragraph(" "));
                document.newPage();

                if (banca.getCoorientador() != null) {
                    //atestado coorientador
                    paragrafo = new Paragraph("Universidade Federal do Pampa", titulo);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    paragrafo = new Paragraph("Campus Alegrete", titulo);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    if (banca.getCurso().equals("CC")) {
                        paragrafo = new Paragraph("Curso de Ciência da Computação", titulo);
                    } else {
                        paragrafo = new Paragraph("Curso de Engenharia de Software", titulo);
                    }
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    document.add(new Paragraph(" "));
                    document.add(new Paragraph(" "));
                    paragrafo = new Paragraph("ATESTADO", titulo);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    document.add(new Paragraph(" "));
                    document.add(new Paragraph(" "));
                    paragrafo = new Paragraph();
                    paragrafo.add(new Phrase("    A coordenação do curso de "
                            + (banca.getCurso().equals("CC") ? "Ciência da Computação" : "Engenharia de Software")
                            + " da Universidade Federal do Pampa atesta, para os devidos fins, que ", texto));
                    paragrafo.add(new Phrase(banca.getCoorientador().getNome(), textoEmDestaque));
                    paragrafo.add(new Phrase(" participou como coorientador" + (banca.getCoorientador().getGenero() == 'a' ? "a" : "")
                            + " da monografia de Trabalho de Conclusão de Curso d" + banca.getAluno().getGenero()
                            + " alun" + banca.getAluno().getGenero() + " ", texto));
                    paragrafo.add(new Phrase(banca.getAluno().getNome(), textoEmDestaque));
                    paragrafo.add(new Phrase(" intitulada \"", texto));
                    paragrafo.add(new Phrase(banca.getTitulo(), textoEmDestaque));
                    paragrafo.add(new Phrase("\" e apresentada no dia " + banca.getData() + ", no Campus Alegrete – "
                            + "UNIPAMPA, para a seguinte banca avaliadora: ", texto));
                    paragrafo.add(new Phrase(banca.getOrientador().getNome() + ", ", texto));
                    if (banca.getCoorientador() != null) {
                        paragrafo.add(new Phrase(banca.getCoorientador().getNome() + ", ", texto));
                    }
                    for (int i = 0; i < banca.getBanca().size() - 1; i++) {
                        paragrafo.add(new Phrase(banca.getBanca().get(i).getNome() + ", ", texto));
                    }
                    paragrafo.add(new Phrase(banca.getBanca().get(banca.getBanca().size() - 1).getNome() + ".", texto));
                    paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
                    document.add(paragrafo);
                    document.add(new Paragraph(" "));
                    paragrafo = new Paragraph(Escritor.getLocalData(banca.getData()), texto);
                    paragrafo.setAlignment(Element.ALIGN_RIGHT);
                    document.add(paragrafo);
                    document.add(new Paragraph(" "));
                    document.add(new Paragraph(" "));
                    document.add(new Paragraph(" "));
                    paragrafo = new Paragraph("___________________________________", texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    paragrafo = new Paragraph("Prof" + (Configuracoes.getGeneroCoordenadorTCC().equals("a") ? "a. " : ". ") + Configuracoes.getNomeCoordenadorTCC(), texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    paragrafo = new Paragraph("Coordenador" + (Configuracoes.getGeneroCoordenadorTCC().equals("a") ? "a" : "") + " de Trabalho de Conclusão de Curso", texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    document.add(new Paragraph(" "));
                    document.add(new Paragraph(" "));
                    paragrafo = new Paragraph("___________________________________", texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    if (banca.getCurso().equals("CC")) {
                        paragrafo = new Paragraph("Prof" + (Configuracoes.getGeneroCoordenadorCC().equals("a") ? "a. " : ". ") + Configuracoes.getNomeCoordenadorCC(), texto);
                        paragrafo.setAlignment(Element.ALIGN_CENTER);
                        document.add(paragrafo);
                        paragrafo = new Paragraph("Coordenador" + (Configuracoes.getGeneroCoordenadorCC().equals("a") ? "a" : "") + " do Curso de Ciência da Computação", texto);
                        paragrafo.setAlignment(Element.ALIGN_CENTER);
                        document.add(paragrafo);
                    } else {
                        paragrafo = new Paragraph("Prof" + (Configuracoes.getGeneroCoordenadorES().equals("a") ? "a. " : ". ") + Configuracoes.getNomeCoordenadorES(), texto);
                        paragrafo.setAlignment(Element.ALIGN_CENTER);
                        document.add(paragrafo);
                        paragrafo = new Paragraph("Coordenador" + (Configuracoes.getGeneroCoordenadorES().equals("a") ? "a" : "") + " do Curso de Engenharia de Software", texto);
                        paragrafo.setAlignment(Element.ALIGN_CENTER);
                        document.add(paragrafo);
                    }

                    document.newPage();
                    document.add(new Paragraph(" "));
                    document.newPage();
                }

                for (Avaliador a : banca.getBanca()) {
                    //atestados dos demais avaliadores
                    paragrafo = new Paragraph("Universidade Federal do Pampa", titulo);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    paragrafo = new Paragraph("Campus Alegrete", titulo);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    if (banca.getCurso().equals("CC")) {
                        paragrafo = new Paragraph("Curso de Ciência da Computação", titulo);
                    } else {
                        paragrafo = new Paragraph("Curso de Engenharia de Software", titulo);
                    }
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    document.add(new Paragraph(" "));
                    document.add(new Paragraph(" "));
                    paragrafo = new Paragraph("ATESTADO", titulo);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    document.add(new Paragraph(" "));
                    document.add(new Paragraph(" "));
                    paragrafo = new Paragraph();
                    paragrafo.add(new Phrase("    A coordenação do curso de "
                            + (banca.getCurso().equals("CC") ? "Ciência da Computação" : "Engenharia de Software")
                            + " da Universidade Federal do Pampa atesta, para os devidos fins, que ", texto));
                    paragrafo.add(new Phrase(a.getNome(), textoEmDestaque));
                    paragrafo.add(new Phrase(" participou como membro da banca"
                            + " da monografia de Trabalho de Conclusão de Curso d" + banca.getAluno().getGenero()
                            + " alun" + banca.getAluno().getGenero() + " ", texto));
                    paragrafo.add(new Phrase(banca.getAluno().getNome(), textoEmDestaque));
                    paragrafo.add(new Phrase(" intitulada \"", texto));
                    paragrafo.add(new Phrase(banca.getTitulo(), textoEmDestaque));
                    paragrafo.add(new Phrase("\" e apresentada no dia " + banca.getData() + ", no Campus Alegrete – "
                            + "UNIPAMPA, para a seguinte banca avaliadora: ", texto));
                    paragrafo.add(new Phrase(banca.getOrientador().getNome() + ", ", texto));
                    if (banca.getCoorientador() != null) {
                        paragrafo.add(new Phrase(banca.getCoorientador().getNome() + ", ", texto));
                    }
                    for (int i = 0; i < banca.getBanca().size() - 1; i++) {
                        paragrafo.add(new Phrase(banca.getBanca().get(i).getNome() + ", ", texto));
                    }
                    paragrafo.add(new Phrase(banca.getBanca().get(banca.getBanca().size() - 1).getNome() + ".", texto));
                    paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
                    document.add(paragrafo);
                    document.add(new Paragraph(" "));
                    paragrafo = new Paragraph(Escritor.getLocalData(banca.getData()), texto);
                    paragrafo.setAlignment(Element.ALIGN_RIGHT);
                    document.add(paragrafo);
                    document.add(new Paragraph(" "));
                    document.add(new Paragraph(" "));
                    document.add(new Paragraph(" "));
                    paragrafo = new Paragraph("___________________________________", texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    paragrafo = new Paragraph("Prof" + (Configuracoes.getGeneroCoordenadorTCC().equals("a") ? "a. " : ". ") + Configuracoes.getNomeCoordenadorTCC(), texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    paragrafo = new Paragraph("Coordenador" + (Configuracoes.getGeneroCoordenadorTCC().equals("a") ? "a" : "") + " de Trabalho de Conclusão de Curso", texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    document.add(new Paragraph(" "));
                    document.add(new Paragraph(" "));
                    paragrafo = new Paragraph("___________________________________", texto);
                    paragrafo.setAlignment(Element.ALIGN_CENTER);
                    document.add(paragrafo);
                    if (banca.getCurso().equals("CC")) {
                        paragrafo = new Paragraph("Prof" + (Configuracoes.getGeneroCoordenadorCC().equals("a") ? "a. " : ". ") + Configuracoes.getNomeCoordenadorCC(), texto);
                        paragrafo.setAlignment(Element.ALIGN_CENTER);
                        document.add(paragrafo);
                        paragrafo = new Paragraph("Coordenador" + (Configuracoes.getGeneroCoordenadorCC().equals("a") ? "a" : "") + " do Curso de Ciência da Computação", texto);
                        paragrafo.setAlignment(Element.ALIGN_CENTER);
                        document.add(paragrafo);
                    } else {
                        paragrafo = new Paragraph("Prof" + (Configuracoes.getGeneroCoordenadorES().equals("a") ? "a. " : ". ") + Configuracoes.getNomeCoordenadorES(), texto);
                        paragrafo.setAlignment(Element.ALIGN_CENTER);
                        document.add(paragrafo);
                        paragrafo = new Paragraph("Coordenador" + (Configuracoes.getGeneroCoordenadorES().equals("a") ? "a" : "") + " do Curso de Engenharia de Software", texto);
                        paragrafo.setAlignment(Element.ALIGN_CENTER);
                        document.add(paragrafo);
                    }

                    document.newPage();
                    document.add(new Paragraph(" "));
                    document.newPage();
                }
            }

            //--------------------------------------------------------------
            //email de convite
            paragrafo = new Paragraph("Destinatários:", textoEmDestaque);
            document.add(paragrafo);
            paragrafo = new Paragraph();
            paragrafo.add(new Phrase(banca.getOrientador().getEmail() + ", "));
            if (banca.getCoorientador() != null) {
                paragrafo.add(new Phrase(banca.getCoorientador().getEmail() + ", "));
            }
            for (Avaliador a : banca.getBanca()) {
                paragrafo.add(new Phrase(a.getEmail() + ", "));
            }
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            paragrafo = new Paragraph("Assunto:", textoEmDestaque);
            document.add(paragrafo);
            paragrafo = new Paragraph();
            if (banca.isTcc1()) {
                paragrafo.add(new Phrase("Defesa de Projeto de TCC - "));
            } else {
                paragrafo.add(new Phrase("Defesa de Monografia de TCC - "));
            }
            paragrafo.add(new Phrase(banca.getAluno().getNome()));
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            paragrafo = new Paragraph("Corpo do texto:", textoEmDestaque);
            document.add(paragrafo);
            paragrafo = new Paragraph("Prezados,", texto);
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            paragrafo = new Paragraph();
            paragrafo.add(new Phrase("Confirmo a defesa de "));
            if (banca.isTcc1()) {
                paragrafo.add(new Phrase("Projeto de Trabalho de Conclusão de Curso "));
            } else {
                paragrafo.add(new Phrase("Monografia de Trabalho de Conclusão de Curso "));
            }
            paragrafo.add(new Phrase("d" + banca.getAluno().getGenero() + " acadêmic" + banca.getAluno().getGenero() + " " + banca.getAluno().getNome() + " do Curso de " + (banca.getCurso().equals("CC") ? "Ciência da Computação" : "Engenharia de Software") + " da UNIPAMPA."));
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            paragrafo = new Paragraph("Título: " + banca.getTitulo());
            document.add(paragrafo);
            paragrafo = new Paragraph("Data: " + banca.getData());
            document.add(paragrafo);
            paragrafo = new Paragraph("Horário: " + banca.getHorario());
            document.add(paragrafo);
            paragrafo = new Paragraph("Local: sala " + banca.getLocal());
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            paragrafo = new Paragraph("O texto integral do trabalho para avaliação está disponível no seguinte link: " + banca.getLink());
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            paragrafo = new Paragraph("Na ocasião da defesa, serão informados os tempos disponíveis para apresentação do trabalho e para considerações dos avaliadores. Os procedimentos de avaliação (com formulário específico) serão informados pela presidência da banca antes da apresentação do trabalho.");
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            paragrafo = new Paragraph("Fico à disposição para quaisquer esclarecimentos adicionais.");
            document.add(paragrafo);
            document.add(new Paragraph(" "));
            paragrafo = new Paragraph("Cordialmente, " + Configuracoes.getNomeCoordenadorTCC());
            document.add(paragrafo);

            document.close();
        }
    }

    public static String getLocalData(String d) {
        String[] data = d.split("/");
        String r = "Alegrete, " + data[0] + " de ";
        switch (Integer.valueOf(data[1])) {
            case 1:
                r += "janeiro de ";
                break;
            case 2:
                r += "fevereiro de ";
                break;
            case 3:
                r += "março de ";
                break;
            case 4:
                r += "abril de ";
                break;
            case 5:
                r += "maio de ";
                break;
            case 6:
                r += "junho de ";
                break;
            case 7:
                r += "julho de ";
                break;
            case 8:
                r += "agosto de ";
                break;
            case 9:
                r += "setembro de ";
                break;
            case 10:
                r += "outubro de ";
                break;
            case 11:
                r += "novembro de ";
                break;
            case 12:
                r += "dezembro de ";

        }
        r += data[2];
        return r;
    }

}
